from setuptools import setup

setup(
    name='pickemon',
    version='1.0.0',
    packages=[''],
    url='https://gitlab.com/francescoracciatti/pickemon',
    license='MIT',
    author='francescoracciatti',
    author_email='racciatti.francesco@gmail.com',
    description='Microservice to gather information about Pokemon'
)
